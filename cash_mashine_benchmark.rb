require 'benchmark'
require 'redis'
require_relative 'cash_machine'

module CashMachineBenchmark
  ITERATIONS_COUNT = 100

  def self.call
    Redis.new.flushall

    Benchmark.bm do |bm|
      bm.report do
        ITERATIONS_COUNT.times do
          CashMachine.new(lat: rand(CashMachine::SOUTH_LAT..CashMachine::NORTH_LAT),
                          lon: rand(CashMachine::WEST_LON..CashMachine::EAST_LON)).nearest
        end
      end
    end
  end
end




