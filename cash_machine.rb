require 'pg'
require 'redis'

class CashMachine
  DATABASE_NAME = 'bank_app'
  LIMIT = 5

  # Approximate Moscow coordinates
  NORTH_LAT = 55.912216
  SOUTH_LAT = 55.575182
  WEST_LON = 37.372919
  EAST_LON = 37.838465

  ITERATIVE_DISTANCE = 0.04
  ITERATIONS_COUNT = 15

  attr_reader :lat, :lon

  def initialize(lat:, lon:)
    @lat = lat
    @lon = lon
  end

  def nearest
    return cached_data if cached_data
    points = []

    ITERATIONS_COUNT.times do |i|
      sql = select_sql(i)
      points = []

      connection.exec(sql) do |result|
        result.each do |row|
          cash_machine_lat = row['lat'].to_f
          cash_machine_lon = row['lon'].to_f

          square_dist = (lat - cash_machine_lat)**2 + (lon - cash_machine_lon)**2
          points << [square_dist, cash_machine_lat, cash_machine_lon]
        end
      end

      break if points.count >= LIMIT
    end

    points = points.sort.first(LIMIT).each { |x| x.delete_at(0) }
    redis.set(cache_key, points)
    points
  end

  private

  def connection
    @connection ||= PG.connect(dbname: DATABASE_NAME)
  end

  def cached_data
    @cached_data ||= begin
      points = redis.get(cache_key)
      JSON.parse(points) unless points.nil?
    end
  end

  def redis
    @redis ||= Redis.new
  end

  def cache_key
    @cache_key ||= [lat.round(4), lon.round(4)].join(';')
  end

  def select_sql(iteration_index)
    distance = ITERATIVE_DISTANCE * (iteration_index + 1)

    <<~SQL
      SELECT * 
      FROM cash_machines 
      WHERE lat > #{lat - distance}
        AND lat < #{lat + distance} 
        AND lon > #{lon - distance}
        AND lon < #{lon + distance}
    SQL
  end
end
