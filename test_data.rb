require 'pg'
require_relative 'cash_machine'

module TestData
  extend self

  RECORDS_COUNT = 100_000

  def create_table
    connection.exec <<~SQL
      CREATE TABLE cash_machines (id bigserial primary key, lat float8 NOT NULL, 
        lon float8 NOT NULL);
      CREATE INDEX lat_idx ON cash_machines(lat);
      CREATE INDEX lon_idx ON cash_machines(lon);
    SQL
  end

  def fill_table
    connection.exec <<~SQL
      INSERT INTO cash_machines
      SELECT i, random() * #{lat_diff} + #{min_lat}, random() * #{lon_diff} + #{min_lon}
      FROM generate_series(1, #{RECORDS_COUNT}) AS i;
    SQL
  end

  private

  def connection
    PG.connect(dbname: CashMachine::DATABASE_NAME)
  end

  def min_lat
    [CashMachine::NORTH_LAT, CashMachine::SOUTH_LAT].min
  end

  def lat_diff
    (CashMachine::NORTH_LAT - CashMachine::SOUTH_LAT).abs
  end

  def min_lon
    [CashMachine::WEST_LON, CashMachine::EAST_LON].min
  end

  def lon_diff
    (CashMachine::WEST_LON - CashMachine::EAST_LON).abs
  end
end
